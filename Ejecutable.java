public class Ejecutable {

    public static void main(String[] args) {
        
        Producto producto1 = new Producto();
        Producto producto2 = new Producto();
        
        producto1.setNombreDelProducto("Pack 100 vasos descartables");
        producto1.setCodigoDelProducto(656743);
        producto1.setPrecioDeCosto(350f);
        producto1.setPorcentajeDeGanancia(0.5f);
        
        producto2.setNombreDelProducto("Pack 100 bolsas de consorcio");
        producto2.setCodigoDelProducto(222431);
        producto2.setPrecioDeCosto(150f);
        producto2.setPorcentajeDeGanancia(0.7f);
        
        producto1.informacionDelProducto();
        producto1.determinaPrecioDeVenta();
        
        producto2.informacionDelProducto();
        producto2.determinaPrecioDeVenta();
    
    }
    
}