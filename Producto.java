public class Producto {

    long codigoDelProducto;
    String nombreDelProducto;
    Float precioDeCosto;
    Float porcentajeDeGanancia;
    Float ivaDelProducto=0.21f;
    Float precioDeVenta;
       
       public void setCodigoDelProducto(long codigoDelProducto){
           this.codigoDelProducto = codigoDelProducto;
       }
       public void setNombreDelProducto(String nombreDelProducto){
           this.nombreDelProducto = nombreDelProducto;
       }
       public void setPrecioDeCosto(Float precioDeCosto){
           this.precioDeCosto = precioDeCosto;
       }
       public void setPorcentajeDeGanancia(Float porcentajeDeGanancia){
           this.porcentajeDeGanancia = porcentajeDeGanancia;
       }
       public void setIVAdelProducto(Float ivaDelProducto){
           this.ivaDelProducto = ivaDelProducto;
       }
       public void setPrecioDeVenta(Float precioDeVenta){
           this.precioDeVenta = precioDeVenta;
       }
       
       public long getCodigoDelProducto(){
           return codigoDelProducto;
       }
       public String getNombreDelProducto(){
           return nombreDelProducto;
       }
       public Float getPrecioDeCosto(){
           return precioDeCosto;
       }
       public Float getPorcentajeDeGanancia(){
           return porcentajeDeGanancia;
       }
       public Float getIVAdelProducto(){
           return ivaDelProducto;
       }
       public Float getPrecioDeVenta(){
           return precioDeVenta;
       }
       
       
       public void informacionDelProducto(){
           
           System.out.println("\n Nombre del producto: "+nombreDelProducto+"\n Codigo del producto: "+codigoDelProducto+"\n El precio de costo es de: $"+precioDeCosto+"\n EL porcentaje de ganancia es de: "+porcentajeDeGanancia+"% \n Y con un IVA del 21%");
       }
       
       public void determinaPrecioDeVenta(){
           
           Float a = null;
           Float b= null;
           
           a = precioDeCosto*ivaDelProducto;
           b = precioDeCosto*porcentajeDeGanancia;
           precioDeVenta = a + b + precioDeCosto;
           
           System.out.println(" El costo del producto es: $"+precioDeVenta);
           
           System.out.println("__________________________________");
       }

}
